/**
 ******************************************************************************
 * @file    main.c
 * @author  Ac6
 * @version V1.0
 * @date    01-December-2013
 * @brief   Default main function.
 ******************************************************************************
 */

#include "stm32f10x.h"

/*
 * Settings with 24MHz cpu
 */

#define ABSOLUTE_PERIOD_MAX 1065		//ROTARY 300mm 2RPM
#define ABSOLUTE_PERIOD_MIN 35			//ROTARY 2000mm 10RPM
#define CALIBRATING_ROTARY_PERIOD 225 	//ROTARY 300mm 10RPM

#define ROTARY_SENSOR_PIN GPIO_Pin_7
#define ROTARY_SENSOR_PORT GPIOA
#define ROTARY_DRIVER_CONTROL_PIN GPIO_Pin_6
#define ROTARY_DRIVER_CONTROL_PORT GPIOA

volatile int target_period = CALIBRATING_ROTARY_PERIOD - 1;
volatile int current_period = CALIBRATING_ROTARY_PERIOD - 1;

volatile int timer_dsec = 0;

volatile int high_state_sensor = 0;

volatile int dead_sensor_dsec = 0;
volatile int dead_sensor_dsec_max = 20;

volatile int round_counter = -1;
volatile int last_round_time = 0;

volatile int prescaler = 8 - 1;

int main(void) {

	GPIO_InitTypeDef gpio;
	TIM_TimeBaseInitTypeDef tim;
	NVIC_InitTypeDef nvic;

	RCC_APB2PeriphClockCmd(
			RCC_APB2Periph_GPIOA |
			RCC_APB2Periph_GPIOC |
			RCC_APB2Periph_GPIOB,
			ENABLE);

	RCC_APB1PeriphClockCmd(
			RCC_APB1Periph_TIM2 |
			RCC_APB1Periph_TIM3,
			ENABLE);

	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = ROTARY_DRIVER_CONTROL_PIN;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(ROTARY_DRIVER_CONTROL_PORT, &gpio);

	TIM_Cmd(TIM3, ENABLE);

	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = ROTARY_SENSOR_PIN;
	gpio.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(ROTARY_SENSOR_PORT, &gpio);

	TIM_TimeBaseStructInit(&tim);
	tim.TIM_CounterMode = TIM_CounterMode_Up;
	tim.TIM_Prescaler = 2400 - 1;
	tim.TIM_Period = 1000 - 1;
	TIM_TimeBaseInit(TIM2, &tim);

	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);

	TIM_Cmd(TIM2, ENABLE);

	nvic.NVIC_IRQChannel = TIM2_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 0;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;

	NVIC_Init(&nvic);

	for (;;) {
	}
}

void TIM2_IRQHandler() {
	if (TIM_GetITStatus(TIM2, TIM_IT_Update) == SET) {
		TIM_ClearITPendingBit(TIM2, TIM_IT_Update);

		int sensor_on = GPIO_ReadInputDataBit(ROTARY_SENSOR_PORT, ROTARY_SENSOR_PIN);

		if(high_state_sensor == 0 && sensor_on){
			//CHANGE FROM LOW TO HIGH
			//set high level of sensor variable and clear deadtime
			//it is probably next rotation
			high_state_sensor = 1;
			dead_sensor_dsec = 0;
			last_round_time = timer_dsec;
			timer_dsec = 0;
			round_counter++;
		}

		if(high_state_sensor == 1 && !sensor_on){
			//CHANGE FROM HIGH TO LOW
			//CHECK IF DEADTIME PASS
			if(dead_sensor_dsec > dead_sensor_dsec_max){
				high_state_sensor = 0;
			}
		}
		timer_dsec++;
		dead_sensor_dsec++;

		if (target_period > current_period){
			current_period++;
		}
		if(target_period < current_period){
			current_period--;
		}
		TIM_TimeBaseInitTypeDef tim;
		TIM_TimeBaseStructInit(&tim);
		tim.TIM_CounterMode = TIM_CounterMode_Up;
		tim.TIM_Prescaler = prescaler;
		tim.TIM_Period = current_period;
		TIM_TimeBaseInit(TIM3, &tim);

		TIM_OCInitTypeDef  channel;

		TIM_OCStructInit(&channel);
		channel.TIM_OCMode = TIM_OCMode_PWM1;
		channel.TIM_OutputState = TIM_OutputState_Enable;
		channel.TIM_Pulse = current_period / 2;
		TIM_OC1Init(TIM3, &channel);

		TIM_Cmd(TIM3, ENABLE);

	}
}
